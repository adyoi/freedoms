-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for freedoms
CREATE DATABASE IF NOT EXISTS `freedoms` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `freedoms`;

-- Dumping structure for table freedoms.freedoms_login
CREATE TABLE IF NOT EXISTS `freedoms_login` (
  `login_id` int(5) NOT NULL AUTO_INCREMENT,
  `login_user_id` tinyint(2) NOT NULL,
  `login_level_id` tinyint(1) NOT NULL,
  `login_status` tinyint(1) NOT NULL,
  `login_username` varchar(32) NOT NULL,
  `login_password` varchar(256) NOT NULL,
  `login_name` varchar(50) NOT NULL,
  `login_email` varchar(50) NOT NULL,
  `login_image` varchar(50) NOT NULL,
  `login_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`login_id`),
  KEY `login_user_id` (`login_user_id`),
  KEY `login_level_id` (`login_level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table freedoms.freedoms_login: ~0 rows (approximately)
/*!40000 ALTER TABLE `freedoms_login` DISABLE KEYS */;
INSERT IGNORE INTO `freedoms_login` (`login_id`, `login_user_id`, `login_level_id`, `login_status`, `login_username`, `login_password`, `login_name`, `login_email`, `login_image`, `login_timestamp`) VALUES
	(1, 0, 1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'admin@admin.com', 'admin.png', '2017-02-19 03:16:34');
/*!40000 ALTER TABLE `freedoms_login` ENABLE KEYS */;

-- Dumping structure for table freedoms.freedoms_login_level
CREATE TABLE IF NOT EXISTS `freedoms_login_level` (
  `login_level_id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `login_level_name` varchar(50) NOT NULL,
  PRIMARY KEY (`login_level_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table freedoms.freedoms_login_level: 1 rows
/*!40000 ALTER TABLE `freedoms_login_level` DISABLE KEYS */;
INSERT IGNORE INTO `freedoms_login_level` (`login_level_id`, `login_level_name`) VALUES
	(1, 'Super Administrator'),
	(2, 'Administrator'),
	(3, 'Moderator'),
	(4, 'User');
/*!40000 ALTER TABLE `freedoms_login_level` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
