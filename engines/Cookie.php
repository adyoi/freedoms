<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Cookie
	{
		public static function set_cookie($bool, $name, $value, $expire, $path = null, $domain = null, $security = null)
		{
			if($bool)
			{

				foreach ($expire as $val)
				{
					$arr[] = $val;
				}
				
				$time = self :: set_time($name, $arr[0], $arr[1], $arr[2], $arr[3]);
				
				if($name !== self :: get_cookie($name))
				{
					setcookie($name, $value, $time, $path, $domain, $security);
				}
			}
			else
			{
				$time = self :: del_time($name);
				setcookie($name, $value, $time, $path, $domain, $security);
			}
		}

		public static function get_cookie($name)
		{
			$thecookie = isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
			return $thecookie;
		}
			
		public static function set_time($cookie, $d = '', $h = '', $m = '', $s = '')
		{
			$time = ($d * $h * $m * $s);
			$cookietime = time() + intval($time);
			Session :: set($cookie, $cookietime);
			return $cookietime;
		}
		
		public static function del_time($cookie)
		{
			$cookietime = Session :: get($cookie);
			return time() - intval($cookietime);
		}
	}
	if(!defined('names')) exit('forbidden access');
}