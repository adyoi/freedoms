<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Routes
	{
		static $source_final;
		
		function __construct()
		{
			/*
			 * $no = 1;
			 * echo $no . "x Routes <br>";
			 * $no++;
			 */
			
			if(empty($_SERVER['QUERY_STRING']))
			{
				$source_temp = substr_replace(urldecode($_SERVER['REQUEST_URI']), pathinfo($_SERVER['SCRIPT_NAME'], PATHINFO_FILENAME), null, strlen($_SERVER['SCRIPT_NAME']));
				
				self :: $source_final = filter_var($source_temp, FILTER_SANITIZE_SPECIAL_CHARS);
			}	
			else
			{
				$source_temp = str_replace(Config :: $config['app']['ext'], null, $_SERVER['QUERY_STRING']);
				
				self :: $source_final = filter_var($source_temp, FILTER_SANITIZE_SPECIAL_CHARS);
			}
		}

		public static function baseurl()
		{
			$server_request_scheme = 'http';
			
			if ( (! empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') || 
				 (! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || 
				 (! empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ) 
			{
				$server_request_scheme = 'https';
			} 
			else 
			{
				$server_request_scheme = 'http';
			}
			
			$windows_or_linux_path = dirname($_SERVER['SCRIPT_NAME']);
			
			if (substr($windows_or_linux_path, -1) == '/')
			{
				$windows_or_linux_path = substr($windows_or_linux_path, 0, strlen($windows_or_linux_path) -1);
			}
			else
			{
				$windows_or_linux_path = dirname($_SERVER['SCRIPT_NAME']);
			}

			return $server_request_scheme . '://' . $_SERVER['SERVER_NAME'] . $windows_or_linux_path;
		}
		
		public static function basepath()
		{
			return self :: $source_final;
		}
		
		public static function gourl($url)
		{
			return rtrim(self :: thisurl(), '/') . '/' . $url . '/';
		}
		
		public static function changeurl($url)
		{
			$source = explode('/', rtrim(self :: $source_final, '/'));
			
			$source_one = array_key_exists(0, $source) ? $source[0] : null;
			$source_two = array_key_exists(1, $source) ? $source[1] : null;
			
			return self :: baseurl() . '/' . $source_one . '/' .  $source_two . '/' . $url . '/';
		}
		
		public static function thisurl()
		{
			return self :: baseurl() . '/' . rtrim(self :: $source_final, '/') . '/';
		}
		
		public static function thispath()
		{
			return explode('/', rtrim(self :: $source_final, '/'));
		}
	}
	if(!defined('names')) exit('forbidden access');
}