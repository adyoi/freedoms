<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Datatable
	{
		public function __construct()
		{	
			;
		}
		
		public static function sqlSearch ($columns)
		{
			$searchs = array();
	
			foreach ($columns as $key => $val)
			{
				$searchs[] = $val["field"];
			}
			
			$array = $_GET["columns"];
			$whr = array();
			$where = "";
			
			foreach ($array as $key => $val)
			{
				if (isset($val["search"]["value"]) && $val["search"]["value"]  !== null)
				{
					$search_value = $val["search"]["value"];
					$find = explode("|", $search_value);
					
					if (is_array($find) && count($find) > 1)
					{
						$where = " WHERE "; // WHERE
						
						$find0 = (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) ? stripslashes($find[0]) : $find[0];
						$find1 = (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) ? stripslashes($find[1]) : $find[1];
						
						switch ($find0)
						{
							case "%%":
							
								$whr[] = $searchs[$key] . " LIKE '%{$find1}%'";
								
								break;
								
							case "%s":
							
								$whr[] = $searchs[$key] . " LIKE '%{$find1}'";
								
								break;
								
							case "s%";
							
								$whr[] = $searchs[$key] . " LIKE '{$find1}%'";
								
								break;
								
							default:
							
								$check1 = $find0 == "null" ? "=" : $find0;
								$check2 = is_numeric($find1) ?  $find1 : "'{$find1}'";
								$whr[] = $searchs[$key] . " {$check1} " . $check2;
						}
					}
				}
			}

			return $where . implode(" AND ", array_filter($whr));
		}
		
		public static function sqlFilter ($columns)
		{
			$order = array();
			
			foreach ($columns as $key => $val)
			{
				if (array_key_exists("alias", $val))
				{
					$order[] = $val["alias"];
				}
				else	
				{	
					$order[] = $val["field"];
				} 
			}
			
			$start = isset($_GET["start"]) ? intval($_GET["start"]) : intval(0);
			$length = isset($_GET["length"]) ? intval($_GET["length"]) : intval(10);
			$orderdir = isset($_GET["order"][0]["dir"]) ? $_GET["order"][0]["dir"] : "desc";
			$ordercolumn = isset($_GET["order"][0]["column"]) ? $_GET["order"][0]["column"] : intval(0);
			
			return " ORDER BY " . $order[$ordercolumn] . " " . $orderdir . " LIMIT " . $start . ", " . $length;
		}
		
		public static function sqlSelect ($columns)
		{
			$fields = array();
			
			foreach ($columns as $key => $val)
			{
				if (array_key_exists("alias", $val))
				{
					$fields[] = $val["field"] . " as " . $val["alias"];
				}
				else	
				{	
					$fields[] = $val["field"];
				}
			}
			
			return implode(", ", $fields);
		}
		
		public static function buildColumn ($columns)
		{
			foreach ($columns as $cols)
			{
				$inject = isset($cols["render"]) ? $cols["render"] : "null";
				$render = ob_get_clean();
				$render = ob_start();
				$render = $inject;
				
				$column[] = '{"searchable" : '. ($cols["search"] == true ? "true" : "false") . 
							', "visible"   : '. ($cols["display"] == true ? "true" : "false") . 
							', "orderable" : '. ($cols["order"] == true ? "true" : "false") . 
							', "data"      : '. (isset($cols["alias"]) ? '"' . $cols["alias"] . '"' : '"' .  $cols["field"] . '"') . 
							', "title"     : "'. $cols["title"] . '", "render" : '. $render . '}';
			}

			return "[" . implode(",", $column) . "]";
		}
		
		public static function buildTable ($id, $columns)
		{
			$dthead = "";
			$dtfilter = "";
			
			foreach ($columns as $cols)
			{
				$dthead .= '<th>' . (isset($cols["title"]) ? $cols["title"] : "") . '</th>';
					  
				if (isset($cols["filter"]))
				{
					if (isset($cols["filter"]["type"]))
					{
						$type = $cols["filter"]["type"];
						$width = isset($cols["filter"]["width"]) ? $cols["filter"]["width"] : "100%";
						$operator = '<div class="fright">
										<select class="operator">
											<option vslue="=">=</option>
											<option vslue="!=">!=</option>
											<option vslue="<>"><></option>
											<option vslue="<"><</option>
											<option vslue="<="><=</option>
											<option vslue=">">></option>
											<option vslue=">=">>=</option>
											<option value="s%">s%</option>
											<option value="%s">%s</option>
											<option value="%%">%%</option>
										</select>
									 </div>';
						
						switch($type)
						{
							case "check":
							
								$html = '<td>
											<div style="width:' . $width . '">
												<input type="checkbox" class="select-all"> 
												<input class="filter" type="hidden" value="">
											</div>
										</td>';
								break;
								
							case "text":
							
								$html = '<td>
											<div style="width:' . $width . '">
												' . $operator . '
												<div class="fleft">
													<input class="filter" type="text">
												</div>
											</div>
										</td>';
								break;
							
							case "label":
							
								$text = '';

								if (isset($cols["filter"]["text"]))
								{
									$text = $cols["filter"]["text"];
								}
							
								$html = '<td>
											<div style="width:' . $width . '">
												' . $operator . '
												<div class="fleft">
													<label class="filter"> ' . $text . '</label>
												</div>
											</div>
										</td>';
								break;
							
							case "select":
							
								$optional = '';
							
								if (isset($cols["filter"]["option"]))
								{
									$option = $cols["filter"]["option"];
									
									foreach ($option as $key => $val)
									{
										$optional .= '<option value="' . $val . '">' .  $key . '</option>';
									}
								}
							
								$html = '<td>
											<div style="width:' . $width . '">
												' . $operator . '
												<div class="fleft">
													<select class="filter" style="height: 26px;">
														<option value="" >Please Choose</option>
														' . $optional . '
													</select>
												</div>
											</div>
										</td>';
								break;
							
							case "action":
							
								$html = '<td>
											<div style="width: ' . $width . '; text-align: center; margin: auto">
												<a href="javascript:void(0)" class="searching btn btn-xs btn-default"><i class="fa fa-search"></i> Search</a>
												<a href="javascript:void(0)" class="resetting btn btn-xs btn-default"><i class="fa fa-reply"></i> Reset</a>
												<input class="filter" type="hidden" value="">
											</div>
										</td>';
								break;
							
							default:
							
								$html = '';
						}
					}
				}
				else
				{
					$html = '<td>
								<div style="width: 100%">
									<input class="filter" type="hidden" value="">
								</div>
							</td>';
				}
				
				$dtfilter .= $html;
			}
			
			$table = '<table id="' . $id . '" class="table table-bordered table-hover table-condensed">
						<thead>
							<tr class="dthead">' . $dthead . '</tr>
							<tr class="dtfilter">' . $dtfilter . '</tr>
						</thead>
					</table>';
			
			return $table;
		}
		
	}
	if(!defined('names')) exit('forbidden access');
}