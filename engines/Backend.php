<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Backend
	{
		public function __construct()
		{	
			;
		}

		public static function page($name, $data)
		{
			$baseurl = Routes :: baseurl();
			$basepath = Routes :: basepath();
			$thisurl = Routes :: thisurl();
			$thispath = Routes :: thispath();
			
			//echo(print_r($thispath));
		
			$login = Session :: get('login');
			$auth = Profile :: data_profile('level');

			$Amenu = $thispath[0];
			$Amenu1 = isset($thispath[1]) ? $thispath[1] : null;
			$Amenu2 = isset($thispath[2]) ? $thispath[2] : null;
			
			$Cmenu = Config :: $menu[$Amenu];
			$menu1 = isset($Cmenu[$Amenu1]['access']) ? $Cmenu[$Amenu1]['access'] : array();
			$menu2 = isset($Cmenu[$Amenu1]['submenu'][$Amenu2]['access']) ? $Cmenu[$Amenu1]['submenu'][$Amenu2]['access'] : array();
			
			$ttitle = isset($Cmenu[$Amenu1]['title']) ? ' - ' . $Cmenu[$Amenu1]['title'] : '';
			$title = Config :: $backend['info']['names'] . $ttitle;
			
			$file1 = isset($thispath[1]) ? $thispath[1] . '/' : '';
			$file = isset($thispath[2]) ? $thispath[2] : 'index';
			
			$what = './app/public/' . $thispath[0] . '/'. $file1 . $file . '.php';

			if (is_array($data) && count($data) > 0)
			{	
				foreach($data as $key => $value)
				{
					$$key = $value;
				}
			}
			
			$data['template'] = ob_start();
			if(file_exists($what))
			{
				if($name == Config :: $backend['controller']['name'])
				{
					if($login == false)
					{
						$data['template'] = include_once './app/public/' . $name . '/user/login.php';
					}
					else
					{
						if(isset($Amenu1))
						{
							if(in_array($auth, $menu1))
							{
								if(isset($Amenu2))
								{
									if(in_array($auth, $menu2))
									{
										$data['template'] = include_once($what);
									}
									else
									{
										$data['template'] = include_once('./app/public/' . $thispath[0] . '/forbidden.php');
									}
								}
								else
								{
									$data['template'] = include_once($what);
								}
							}
							else
							{
								$data['template'] = include_once('./app/public/' . $thispath[0] . '/forbidden.php');
							}
						}
					}
				}
			}
			else
			{
				if($name == Config :: $backend['controller']['name'])
				{
					if($login == false)
					{
						$data['template'] = include_once './app/public/' . $name . '/user/login.php';
					}
					else
					{
						$data['template'] = include_once('./app/public/' . $thispath[0] . '/error.php');
					}
				}
			}
			$data['template'] = ob_get_clean();
			
			$data['js'] = isset($js) ? $js : null;
			$data['css'] = isset($css) ? $css : null;
			$data['jqueryReady'] = isset($jqueryReady) ? $jqueryReady : null;
			
			$data['status'] = isset($login) ? 'Username or Password is invalid' : 'Sign in to start your session';
			
			if (is_array($data) && count($data) > 0)
			{	
				foreach($data as $key => $value)
				{
					$$key = $value;
				}
			}

			if($name == Config :: $backend['controller']['name'])
			{
				if($login == false)
				{
					require_once './app/template/admin_login.php';
				}
				else
				{
					require_once './app/template/' . $name . '.php';
				}
			}
		}
	}
	if(!defined('names')) exit('forbidden access');
}