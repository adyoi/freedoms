<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Controller
	{
		function __construct ()
		{
			/* 
			 * $no = 1;
			 * echo $no . "x Controller <br>";
			 * $no++;
			 */
			
			$source = array();
			$source = explode ('/', rtrim(Routes :: $source_final, '/'));
			
			$base = array_key_exists (0 , $source) ? $source[0] : null;
			$param = array_key_exists (1 , $source) ? $source[1] : null;
			
			$classfile = Config :: $config['controller']['path'] . $base . Config :: $config['app']['ext'];
			$classname = __namespace__  . '\\' . ucfirst ($base);
			
			$controller = (object) array();
			$class_controller = AutoLoad :: check_class_file ($classfile);
			
			if ($class_controller)
			{
				$controller = AutoLoad :: check_class_name ($classname);
			}
			
			$param_source = array();

			for ($i = 2; $i < count($source); $i++)
			{
				if ($i)
				{
					$param_source[] = array_key_exists ($i , $source) ? $source[$i] : null;
				}
			}

			if (method_exists ($controller, $param))
			{
				$local_function = array();
				$local_function = isset(get_class_vars(get_class($controller))["function"]) ? get_class_vars(get_class($controller))["function"] : array();
				
				$class_function = array();
				$class_function = get_class_methods($controller);
				
				$check_function = array();
				$check_function = array_diff($class_function, $local_function);
				
				if (in_array($param, $check_function))
				{
					$controller -> {$param}($param_source);
				}
				else
				{
					exit ('<b style="color:#dcdcdc">Controller [' . $classname . '] : (' . $param . ') function is local</b>');
				}
			}
			else
			{
				if (method_exists ($controller, 'index'))
				{
					$controller -> index(); 
				}
				else
				{
					exit ('<b style="color:#dcdcdc">Controller [' . $classname . '] : (index) function not exist</b>');
				}
			}
		}
	}
	if(!defined('names')) exit('forbidden access');
}