<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Handler
	{
		public function __construct ()
		{	
			register_shutdown_function(array($this,'register_shutdown'));
			set_error_handler(array($this, 'set_error'));
		}
		
		function register_shutdown () 
		{
			$rs_err = array();
			
			if ($rs_err = error_get_last())
			{
				$rs_srr = self :: switch_error ($rs_err);
				$rs_msg = self :: message_error (
					$rs_err['type'], 
					$rs_err['message'], 
					$rs_err['line'], 
					$rs_err['file']
				);
				
				echo sprintf($rs_msg, $rs_srr . '[s]');
			}
		}
		
		function set_error ($errno, $errstr, $errfile, $errline)
		{
			$se_err = true;
			
			if (!(error_reporting() & $errno))
			{
				$se_err = false;
			}
			
			$se_srr = self :: switch_error ($errno);
			$se_msg = self :: message_error ($errno, $errstr, $errline, $errfile);
			
			echo sprintf($se_msg, $se_srr);
			return $se_err;
		}
		
		public static function message_error ($type, $message, $line, $file)
		{
			$text = null;
			$html = ob_start();
			$html = ob_get_clean();
			$html = ob_get_contents();
			
			if ($html != strip_tags($html))
			{
				$text = "<div style='padding:10px;border:1px solid #795548;color:#795548;margin:20px;text-align:left;font-family:courier'>
						<b>" . names . " ~ %s</b><br />
						Error [{$type}]: {$message} <br />
						Line: <b>{$line}</b> <br />
						File: {$file}
					</div>";
			}
			else
			{
				header('Content-Type: text/html; charset=utf-8');
				$text = "<!doctype html>
						<html>
							<head><title>" . names . " - Error</title></head>
						<body>
							<div style='padding:10px;border:1px solid #795548;color:#795548;margin:20px;text-align:left;font-family:courier'>
								<b>" . names . " ~ %s</b><br />
								Error [{$type}]: {$message} <br />
								Line: <b>{$line}</b> <br />
								File: {$file}
							</div>
						</body>
						</html>";
			}
			return $text;
		}
		
		public static function switch_error ($error)
		{
			$sw_msg = null;
			switch ($error)
			{
				case E_ERROR:
				case E_CORE_ERROR:
				case E_COMPILE_ERROR:
					$sw_msg = 'Fatal Error';
					break;
				case E_USER_ERROR:
				case E_RECOVERABLE_ERROR:
					$sw_msg = 'Error';
					break;
				case E_WARNING:
				case E_CORE_WARNING:
				case E_COMPILE_WARNING:
				case E_USER_WARNING:
					$sw_msg = 'Warning';
					break;
				case E_NOTICE:
				case E_USER_NOTICE:
					$sw_msg = 'Notice';
					break;
				case E_PARSE:
				case E_STRICT:
					$sw_msg = 'Debug';
					break;
				default:
					$sw_msg = 'Unknown Error';
					break;
			}
			return $sw_msg;
		}
		
		public static function development ($bolean = true)
		{
			if ($bolean == true)
			{
				error_reporting (E_WARNING | E_NOTICE);
			}
			else
			{
				error_reporting(0);
			}
		}
	}
	if(!defined('names')) exit('forbidden access');
}