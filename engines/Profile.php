<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Profile
	{
		public function __construct()
		{
			;
		}
		
		public static function data_login($username, $password)
		{
			$dp = Profile :: data_db('admin');
			$db = Database_mysqli :: opendb($dp);

			if(!$db == null)
			{
				$username = Database_mysqli :: escape($username);
				$password = Database_mysqli :: escape($password);
				
				$sql = sprintf("SELECT 
									`login_username`, 
									`login_password` 
								FROM 
									`freedoms_login` 
								WHERE 
									`login_username` = '%s' 
								AND 
									`login_password` = md5('%s')", $username, $password);
				
				$data = Database_mysqli :: query($sql);

				if(count($data) > 0)
				{
					Session :: set('login', true);
					Session :: set('username', $data[0]['login_username']);
					Session :: set('password', $data[0]['login_password']);
				}
				else
				{
					Session :: set('login', false);
					Session :: set('username', "");
					Session :: set('password', "");
				}
				
				Database_mysqli :: closedb();
			}
		}
		
		public static function data_profile($key = null)
		{
			$what = '[ No profile ]';
			
			$dp = Profile :: data_db('admin');
			$db = Database_mysqli :: opendb($dp);
			
			$username = Session :: get('username');
			$password = Session :: get('password');

			if(!$db == null)
			{
				$username = Database_mysqli :: escape($username);
				$password = Database_mysqli :: escape($password);
				
				$sql = sprintf("SELECT * 
								FROM 
									freedoms_login A
								INNER JOIN 
									freedoms_login_level B
								ON 
									A.login_level_id = B.login_level_id
								WHERE 
									A.login_username = '%s' 
								AND 
									A.login_password = '%s'", $username, $password);

				$data = Database_mysqli :: query($sql);
				
				if(count($data) > 0)
				{
					switch($key)
					{
						case 'id':
							$what = $data[0]['login_id'];
						break;
						case 'id_user':
							$what = $data[0]['login_user_id'];
						break;
						case 'user':
							$what = $data[0]['login_username'];
						break;
						case 'name':
							$what = $data[0]['login_name'];
						break;
						case 'division':
							$what = $data[0]['login_division'];
						break;
						case 'level':
							$what = $data[0]['login_level_name'];
						break;
						case 'email':
							$what = $data[0]['login_email'];
						break;
						case 'status':
							$what = $data[0]['login_status'];
						break;
						case 'image':
							$what = $data[0]['login_image'];
						break;
						default:
							$what = '[ Not Available ]';
					}
				}
				else
				{
					$what = '[ Not Authorized ]';
				}

				Database_mysqli :: closedb();
			}
			return $what;
		}
		
		public static function data_db($name)
		{
			$data = array(
				'host' => (isset(Config :: $db[$name]['host']) ? Config :: $db[$name]['host'] : null),
				'dbas' => (isset(Config :: $db[$name]['dbas']) ? Config :: $db[$name]['dbas'] : null),
				'user' => (isset(Config :: $db[$name]['user']) ? Config :: $db[$name]['user'] : null),
				'pass' => (isset(Config :: $db[$name]['pass']) ? Config :: $db[$name]['pass'] : null),
				'type' => (isset(Config :: $db[$name]['type']) ? Config :: $db[$name]['type'] : null),
			);
			
			return $data;
		}
		
		public static function build_subtitle($active)
		{
			$activeMenu = $active[0];
			$configMenu = Config :: $menu[$activeMenu];
			
			$title = '';
			$subtitle = '';
			
			if(isset($active[1]) && isset($configMenu[$active[1]]['title']))
			{
				$title = $configMenu[$active[1]]['title'];
				$subtitle = isset($configMenu[$active[1]]['subtitle']) ? $configMenu[$active[1]]['subtitle'] : '';
				
				if(isset($active[2]) && isset($configMenu[$active[1]]['submenu'][$active[2]]['title']))
				{
					$title = $configMenu[$active[1]]['submenu'][$active[2]]['title'];
					$subtitle = isset($configMenu[$active[1]]['submenu'][$active[2]]['subtitle']) ? $configMenu[$active[1]]['submenu'][$active[2]]['subtitle'] : '';
				}
			}
		
			echo  $title . '<small>' . $subtitle . '</small>';
		}
		
		public static function build_menu($active)
		{
			$activeMenu = $active[0];
			$configMenu = Config :: $menu[$activeMenu];
			$pathMenu = Routes :: baseurl() . '/' . $activeMenu;
			
			$auth = Profile :: data_profile('level');

			foreach($configMenu as $key => $value)
			{
				$state1 = '';
				$state2 = '';
				
				$link = $pathMenu . '/' . $key . '/';
				
				if(isset($active[1]) && $key == $active[1])
				{
					$state1 = 'active';
					$state2 = 'style="display: block;"';
				}

				if(!isset($value['submenu']))
				{
					if(in_array($auth, $value['access']))
					{
						echo '<li class="' . $state1 . '"><a href="' . $link . '"><i class="fa ' . $value['icon'] . '"></i> <span>' . $value['title'] . '</span></a></li>';
					}
				}
				else
				{
					if(in_array($auth, $value['access']))
					{
						echo '<li class="treeview ' . $state1 . '">
							  <a href="#">
								<i class="fa ' . $value['icon'] . '"></i> <span>' . $value['title'] . '</span>
								<span class="pull-right-container">
								  <i class="fa fa-angle-left pull-right"></i>
								</span>
							  </a>
							  <ul class="treeview-menu" ' . $state2 . '>';

								foreach($value['submenu'] as $key1 => $value1)
								{
									$state3 = '';
									
									$link = $pathMenu . '/' . $key . '/' . $key1 . '/';
											
									if(isset($active[2]) && isset($active[1]) && $key1 == $active[2] && $key == $active[1])
									{
										$state3 = 'active';
									}

									if($value1['visible'] == true)
									{
										if(in_array($auth, $value1['access']))
										{
											echo '<li class="' . $state3 . '"><a href="' . $link . '"><i class="fa ' . $value1['icon'] . '"></i> ' . $value1['title'] . '</a></li>';
										}
									}
								}
							  
						echo '</ul>
							  </li>';
							  
					}
				}
			}
		}
	
		public static function build_breadcumb()
		{
			//<li><a href="#">Tables</a></li>
			//<li class="active">Data tables</li>
		}
	}
	if(!defined('names')) exit('forbidden access');
}