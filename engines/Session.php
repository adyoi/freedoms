<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Session
	{
		function __construct()
		{	
			@session_name(strtolower(Config :: $config["session"]["name"]));
			@session_start();
		}
		
		public static function set ($key,$value)
		{	
			$_SESSION[$key] = $value;
			$_SESSION['REMOTE_ADDR'] = md5($_SERVER['REMOTE_ADDR']);
		}

		public static function get ($key)
		{
			if (isset($_SESSION['REMOTE_ADDR']))
			{
				if ($_SESSION['REMOTE_ADDR'] == md5($_SERVER['REMOTE_ADDR']))
				{
					if(isset($_SESSION[$key]))
					{
						return $_SESSION[$key];
					}
				}	
			}
			else
			{
				$_SESSION['REMOTE_ADDR'] = md5($_SERVER['REMOTE_ADDR']);
			}
		}

		public static function init ()
		{
			@session_unset();
			@session_destroy();
			@session_regenerate_id();
		}
	}
	if(!defined('names')) exit('forbidden access');
}