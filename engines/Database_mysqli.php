<?php

namespace Freedoms
{

use mysqli;
use Exception;
	
	class Database_mysqli
	{
		static $mysqli;

		public static function opendb($con)
		{
			try
			{
				self :: $mysqli = @mysqli_connect($con['host'], $con['user'], $con['pass'], $con['dbas']);
				
				if(is_object(self :: $mysqli))
				{
					return self :: $mysqli;
				}
				else
				{
					throw new Exception('[ Check DB Config ]');
				}
			}
			catch(Exception $e)
			{
				print '<b style="color:red">' . $e->getMessage() . '</b>';
			}

		}
		
		public static function errordb()
		{
			return mysqli_error(self :: $mysqli);
		}
		
		public static function closedb()
		{
			return mysqli_close(self :: $mysqli);
		}
		
		public static function escape($string)
		{
			return mysqli_real_escape_string(self :: $mysqli, $string);
		}
		
		public static function lastid()
		{
			return mysqli_insert_id(self :: $mysqli);
		}
		
		public static function query($sql)
		{
			$data = array();
			$result = mysqli_query(self :: $mysqli, $sql);
			
			if($result)
			{
				while($row = mysqli_fetch_assoc($result))
				{
					$data[] = $row;
				}
			}
			else
			{
				$data = false;
			}
			
			return $data;
		}
		
		public static function update($sql)
		{
			if (mysqli_query(self :: $mysqli, $sql) == true)
				return true;
			return false;
		}
	}
	if(!defined('names')) exit('forbidden access');
}