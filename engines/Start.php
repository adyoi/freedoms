<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Start
	{
		public function __construct()
		{
			/* Load Config */
			AutoLoad :: load('config\Config');
			
			/* Load Rules */
			AutoLoad :: load('config\Rules');

			/* Load Routes */
			AutoLoad :: load('engines\Routes');
			
			/* Load Controller */
			AutoLoad :: load('engines\Controller');
		}
	}
	if(!defined('names')) exit('forbidden access');
}