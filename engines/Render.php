<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Render
	{
		public function __construct()
		{	
			;
		}

		public static function page($name, $data)
		{
			$config_view_path = Config :: $config['view']['path'];
			$config_app_ext = Config :: $config['app']['ext'];
			$config_template_path = Config :: $config['template']['path'];
			$config_template_name_index = Config :: $config['template']['name']['index'];
			
			if (is_array($data) && count($data) > 0)
			{	
				foreach($data as $key_view => $value_view)
				{
					$$key_view = $value_view;
				}
			}
			
			$data['template'] = ob_start();
			$data['template'] = include_once $config_view_path . $name . $config_app_ext;
			$data['template'] = ob_get_clean();

			$data['js'] = isset($js) ? $js : null;
			$data['css'] = isset($css) ? $css : null;
			$data['jqueryReady'] = isset($jqueryReady) ? $jqueryReady : null;

			if (is_array($data) && count($data) > 0)
			{	
				foreach($data as $key_template => $value_template)
				{
					$$key_template = $value_template;
				}
			}
			
			require_once $config_template_path . $config_template_name_index . $config_app_ext;
		}
	}
	if(!defined('names')) exit('forbidden access');
}