<?php

namespace Freedoms
{
	
use PDO;

	class Database_pdo extends PDO
	{
		function __construct()
		{
			;
		}
		
		function opendb($con)
		{
			$options = array(
				PDO::ATTR_PERSISTENT    => true,
				PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION    
			);
			
			$dbformat = $con['type'] . ':host=' . $con['host'] . ';dbname=' . $con['dbas'];
			
			try
			{
				return new PDO($dbformat, $con['user'], $con['pass'], $options);
			}
			catch(\PDOException $e)
			{
				print '<b style="color:red">' . $e->getMessage() . '</b>';
			}
		}
		
		public static function usedb($db)
		{
			$cons = new Database;
			return $cons->opendb($db);
		}
		
		public static function profile($name)
		{
			$data = array(
				'host'=> Config::$db[$name]['host'],
				'dbas'=> Config::$db[$name]['dbas'],
				'user'=> Config::$db[$name]['user'],
				'pass'=> Config::$db[$name]['pass'],
				'type'=> Config::$db[$name]['type'],
			);
			
			return $data;
		}
	}
	if(!defined('names')) exit('forbidden access');
}