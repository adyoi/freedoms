<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class AutoLoad
	{
		public static function load ($load)
		{
			/* 
			 * echo $load . ' Loaded <br>';
			 */
			
			$parts = array();
			$parts = explode('\\', $load);
			
			$part_one = array_key_exists(0, $parts) ? $parts[0] : null;
			$part_two = array_key_exists(1, $parts) ? $parts[1] : null;
			
			$classfile = $part_one . '/' . $part_two . '.php';
			$classname = __namespace__ . '\\' . $part_two;
			
			$is_class = self :: check_class_file ($classfile);
			
			if ($is_class)
			{
				self :: check_class_name ($classname);
			}
		}
		
		public static function check_class_file ($file)
		{
			$is_require = false;
			if (!file_exists($file))
			{
				exit ('<b style="color:#dcdcdc">AutoLoad [' . $file . '] file not exist</b>');
			}
			else
			{
				$is_require = true; require_once ($file);
			}
			return $is_require;
		}
		
		public static function check_class_name ($name)
		{
			if (!class_exists($name))
			{
				exit ('<b style="color:#dcdcdc">AutoLoad [' . $name . '] class not exist</b>');
			}
			else
			{
				return new $name;
			}
		}
	}
	if (!defined('names')) exit ('forbidden access');
}