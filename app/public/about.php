<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class About
	{
		public function __construct()
		{	
			AutoLoad :: load ('engines\Render');
		}
		
		public static function index()
		{
			Render :: page('about', null);
		}
		
		public static function history()
		{
			Render :: page('history', null);
		}
	}
	if(!defined('names')) exit('forbidden access');
}