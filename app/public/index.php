<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Index
	{
		var $function = array("greets");
		
		public function __construct()
		{	
			AutoLoad :: load('engines\Render');
		}
		
		public static function index()
		{
			Render :: page('index', null);
		}
		
		public static function login ($arg)
		{
			Render :: page('login', null);
		}
	
		public static function param ($arg)
		{	
			print_r ($arg);
		}
		
		public static function greets ($msg)
		{
			echo $msg;
		}
		
		public static function signin()
		{
			self :: get_login ($_POST["login"], $_POST["password"]);
			
			if (Session :: get('loggedIn') == true)
			{
				Request :: header_url(200, Routes :: baseurl() . '/index/login');
			}
			else
			{
				Request :: header_url(200, Routes :: baseurl() . '/index/login');
			}
		}
		
		public static function signout()
		{
			Session :: init();
			
			Request :: header_url(200, Routes :: baseurl() . '/index/login');
		}
		
		public static function get_login ($username, $password)
		{
			if ($username == "demo" && $password == "demo")
			{
				Session :: set('loggedIn', true);
				Session :: set('errorLogin', false);
			}
			else
			{
				Session :: set('loggedIn', false);
				Session :: set('errorLogin', true);
			}
		}
	
	}
	if(!defined('names')) exit('forbidden access');
}