<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Admin
	{
		public function __construct()
		{	
			AutoLoad :: load ('engines\Datatable');
			AutoLoad :: load ('engines\Profile');
			AutoLoad :: load ('engines\Backend');
			AutoLoad :: load ('engines\Cookie');
		}
		
		public static function index()
		{
			Backend :: page('admin', null);
			Session :: set('last_path', Routes :: thisurl());
		}
		
		public static function signin($menu)
		{
			$username = array_key_exists('username', $_POST) ? $_POST['username'] : '';
			$password = array_key_exists('password', $_POST) ? $_POST['password'] : '';
			
			$last_path = Cookie :: get_cookie("last_path");
			
			Profile :: data_login($username, $password);
			
			if ( Session::get('login') == true)
			{
				Request :: header_url(200, (isset($last_path) ? $last_path : Routes :: baseurl() . "/admin/dashboard/"));
			}
			else
			{
				Request :: header_url(200, Routes :: baseurl() . "/admin/");
			}
		}
		
		public static function signout($menu)
		{
			Cookie :: set_cookie(true, "last_path", Session :: get('last_path'), array(1,1,1800,1));
			
			Session :: init();
			Request :: header_url(200, Routes :: baseurl() . "/admin/");
		}
		
		public static function alive()
		{
			/*
			$time = $_SERVER["REQUEST_TIME"];
			$timeout_duration = 100;
			if (isset($_SESSION["LAST_ACTIVITY"]) && ($time - $_SESSION["LAST_ACTIVITY"]) > $timeout_duration) {
			  session_unset();     
			  session_destroy();
			  session_start();    
			}
			$_SESSION["LAST_ACTIVITY"] = $time;
			
			echo $_SERVER["REQUEST_TIME"];
			echo '<br>';
			echo $_SESSION["LAST_ACTIVITY"];
			*/
		}

	}
	if(!defined('names')) exit('forbidden access');
}