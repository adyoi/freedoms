<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

$js = array();

$css = array();

$jqueryReady = <<<EOL

EOL;
?>

<div class="jumbotron">
  <h1>Freedoms</h1>
  <p>Simply and Powerful PHP Class</p>
  <p><a href="https://gitlab.com/adyoi/freedoms/repository/archive.zip?ref=master" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-star"></span> Download v.0.0.1</a></p>
</div>

<div class="panel panel-info">
  <div class="panel-heading">Directory Tree Structure</div>
  <div class="panel-body">
	<pre style="text-align:left;font-family:courier">
	Freedoms
		|
		└-- app
		|	|
		|	└-- assets
		|	|	|
		|	|	└-- dist (Backend)
		|	|	|	|
		|	|	|	└-- css
		|	|	|	└-- img
		|	|	|	└-- js
		|	|	|	
		|	|	└-- main (Frontend)
		|	|	|	└-- css
		|	|	|	└-- fonts
		|	|	|	└-- images
		|	|	|	└-- js
		|	|	|
		|	|	└-- plugins
		|	|		|
		|	|		└-- (JQuery etc.)
		|	|
		|	└-- libs
		|	|	|
		|	|	└-- (PHP Library etc.)
		|	|
		|	└-- public (Controller)
		|	|	|
		|	|	└-- admin (Backend View and Menu)
		|	|	|	|
		|	|	|	└-- account
		|	|	|	└-- dashboard
		|	|	|	└-- user
		|	|	|
		|	|	└-- index (Frontend View)
		|	|	
		|	└-- template
		|
		└-- config
		└-- engine
	</pre>	
  </div>
</div>

<div class="panel panel-success">
  <div class="panel-heading">Controller Installation</div>
  <div class="panel-body">
	<pre style="text-align:left">&lt;?php

	/* 
	 *	Freedoms version 0.0.1
	 */
	 
	namespace Freedoms
	{
		class Index
		{
			var $function = array("greets");
			
			public function __construct()
			{	
				AutoLoad :: load('engines\Render');
			}
			
			public static function index()
			{
				Render :: page('index', null);
			}
		
			public static function param ($arg)
			{	
				print_r ($arg);
			}
			
			public static function greets ($msg)
			{
				echo $msg;
			}
			
		}
		if(!defined('names')) exit('forbidden access');
	}
	</pre>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">Content</div>
  <div class="panel-body" style="overflow-y:hidden">
	<pre style="text-align:left">&lt;b&gt;&lt;i&gt;&lt;?php index :: greets(base64_decode(&#39;SGVsbG8gV29ybGQ=&#39;)); ?&gt;&lt;/i&gt;&lt;/b&gt;</pre>
	<div style="text-align:left"><b><i><?php index :: greets(base64_decode('SGVsbG8gV29ybGQ='));?></i></b></div>
  </div>
</div> 

<div class="panel panel-default">
  <div class="panel-heading">Controller Usage</div>
  <div class="panel-body" style="overflow-y:hidden">
	<table class="parameter">
		<tr><td>Example 1 </td><td>:</td><td><a href="<?=Routes :: baseurl();?>/index/param/1/2/3/4/5"><?=Routes :: baseurl();?>/index/param/1/2/3/4/5</a></td></tr>
		<tr><td>Example 2 </td><td>:</td><td><a href="<?=Routes :: baseurl();?>/index.php/param/1/2/3/4/5"><?=Routes :: baseurl();?>/index.php/param/1/2/3/4/5</a></td></tr>
		<tr><td>Example 3 </td><td>:</td><td><a href="<?=Routes :: baseurl();?>/index/greets/messages"><?=Routes :: baseurl();?>/index/greets/messages</a></td></tr>
		<tr><td>Example 4 </td><td>:</td><td><a href="<?=Routes :: baseurl();?>/index.php/greets/messages"><?=Routes :: baseurl();?>/index.php/greets/messages</a></td></tr>
	</table>
  </div>
</div>