<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

$js = array();

$css = array();

$jqueryReady = <<<EOL

alert("New Update Release !");

EOL;
?>

<ul class="list-group">
  <li class="list-group-item active">Version 0.0.1 on February 2017</li>
  <li class="list-group-item">1. Admin Panel Dashboard</li>
  <li class="list-group-item">2. Andvance Search</li>
  <li class="list-group-item">3. Optimization Script Code</li>
  <li class="list-group-item">4. Fix More Bugs</li>
</ul>

<ul class="list-group">
  <li class="list-group-item active">Version 1.2 Beta on November 2016</li>
  <li class="list-group-item">1. Admin Panel Dashboard</li>
  <li class="list-group-item">2. Optimization Script Code</li>
  <li class="list-group-item">3. Fix More Bugs</li>
</ul>

<ul class="list-group">
  <li class="list-group-item active">Version 1.1 Beta on January 2015</li>
  <li class="list-group-item">1. With PDO Database Drivers</li>
  <li class="list-group-item">2. Bootstrap Default Viewer</li>
  <li class="list-group-item">3. Array Parameter Controller</li>
  <li class="list-group-item">4. Optimization Script Code</li>
  <li class="list-group-item">5. Fix More Bugs</li>
</ul>

<ul class="list-group">
  <li class="list-group-item active">Version 1.0 Beta on November 2014</li>
  <li class="list-group-item">1. With PDO Database Drivers</li>
  <li class="list-group-item">2. Bootstrap Default Viewer</li>
  <li class="list-group-item">3. Array Parameter Controller</li>
  <li class="list-group-item">4. Optimization Script Code</li>
</ul>

<ul class="list-group">
  <li class="list-group-item active">Version 1.0 Alpha on October 2014</li>
  <li class="list-group-item">1. With PDO Database Drivers</li>
  <li class="list-group-item">2. Bootstrap Default Viewer</li>
</ul>