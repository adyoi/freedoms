<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

$js = array();

$css = array();

$jqueryReady = <<<EOL

EOL;
?>

<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail" style="padding:25px 0 0;">
      <img src="<?=Routes :: baseurl();?>/app/assets/main/images/History.png" alt="...">
      <div class="caption">
        <h3>History</h3>
        <p>Latest Version<br>Version 0.0.1 on February 2017</p>
		<p><a href="<?=Routes :: baseurl();?>/about/history" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-pencil"></span> Change Logs</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail" style="padding:25px 0 0;">
      <img src="<?=Routes :: baseurl();?>/app/assets/main/images/FreedomFrameworks.png" alt="...">
      <div class="caption">
        <h3>Freedoms</h3>
        <p>Freedoms is Simply and Powerful<br>PHP Class</p>
		<p><a href="https://github.com/adyoi/Freedoms" class="btn btn-success" role="button" target="_blank"><span class="glyphicon glyphicon-search"></span> View on Github</a></p>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail" style="padding:25px 0 0;">
      <img src="<?=Routes :: baseurl();?>/app/assets/main/images/AboutMe.png" alt="...">
      <div class="caption">
        <h3>About Me</h3>
        <p>Adi Apriyanto<br>Born at Jakarta, Indonesia</p>
		<p><a href="https://twitter.com/adyoi" class="btn btn-success" role="button" target="_blank"><span class="glyphicon glyphicon-user"></span> Twitter</a></p>
      </div>
    </div>
  </div>
</div>

<div clas="row">
	<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br /><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Freedoms PHP Class</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="<?=Routes :: baseurl();?>" property="cc:attributionName" rel="cc:attributionURL">Adi Apriyanto</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
</div>