<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

$js = array();

$css = array();

$jqueryReady = <<<EOL

EOL;
?>

<div class="content">
<?php if(Session :: get('errorLogin') == true):?>
	<div class="alert alert-danger alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		Invalid Username or Password
	</div>
<?php endif;?>
<?php if(Session :: get('loggedIn') == false):?>
	<h1>Login Demo</h1>
	<p>Username : demo | Password : demo</p>
	<form class="form-horizontal" role="form" action="<?=Routes :: baseurl()?>/index/signin" method="post">
	  <div class="form-group">
		<div class="input-group col-sm-offset-4 col-sm-4">
		  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
		  <input type="text" class="form-control" id="inputEmail3" placeholder="Username" name="login" required >
		</div>
	  </div>
	  <div class="form-group">
		<div class="input-group col-sm-offset-4 col-sm-4">
		  <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
		  <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password" required >
		  
		</div>
	  </div>
	  <div class="form-group">
		<div class="col-sm-offset-4 col-sm-4">
		  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-check"></span> Sign in</button>
		</div>
	  </div>
	</form>
<?php else:?>
	<div class="alert alert-success" role="alert">Login Successfull</div>
<?php endif;?>
</div>