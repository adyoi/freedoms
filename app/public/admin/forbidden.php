<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

?><div class="error-page">
	<h2 class="headline text-yellow"> 403</h2>
	<div class="error-content">
		<h3><i class="fa fa-warning text-yellow"></i> Forbidden Access !</h3>
		<p>
			You do not have an authority to acess this page.
		</p>
		<form class="search-form">
			<div class="input-group">
				<input type="text" name="search" class="form-control" placeholder="Search">
				<div class="input-group-btn">
					<button type="submit" name="submit" class="btn btn-default btn-flat"><i class="fa fa-search"></i>
					</button>
				</div>
			</div>
			<!-- /.input-group -->
		</form>
	</div>
	<!-- /.error-content -->
</div>