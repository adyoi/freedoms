<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

$db = Config :: $db["admin"]["dbas"];
$database = Database_mysqli::opendb( Profile::data_db("admin") );

$sql = sprintf("SELECT 
					engine AS `Engine`,
					table_name AS `Table`,
					table_rows AS `Rows`,
					round(((data_length + index_length) / 1024 / 1024), 2) as `Size`   
				FROM 
					information_schema.TABLES  
				WHERE 
					`table_schema` = '%s'  
				ORDER BY 
					Size desc;"
		, $db
);

$dataTable = Database_mysqli :: query($sql);

$css = array();

$js = array();

$jqueryReady = <<<EOF

EOF;

?>

<!-- Info boxes -->
<div class="row">
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
	<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

	<div class="info-box-content">
	  <span class="info-box-text">CPU Traffic</span>
	  <span class="info-box-number">90<small>%</small></span>
	</div>
	<!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
	<span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

	<div class="info-box-content">
	  <span class="info-box-text">Likes</span>
	  <span class="info-box-number">41,410</span>
	</div>
	<!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->

<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>

<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
	<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

	<div class="info-box-content">
	  <span class="info-box-text">Sales</span>
	  <span class="info-box-number">760</span>
	</div>
	<!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
	<span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

	<div class="info-box-content">
	  <span class="info-box-text">New Members</span>
	  <span class="info-box-number">2,000</span>
	</div>
	<!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Database Information</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="table-responsive">
							<table class='table table-bordered'>
								<tr>
									<th>Name</th>
									<th>Value</th>
								</tr>
								<?php
								foreach($database as $key => $value)
								{
									echo "<tr><td>" . $key . "</td><td>" . (is_array($database->$key) ? implode(', ', $database->$key) : $database->$key) . "</td></tr>";
								}
								?>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="table-responsive" style="height:800px;overflow:auto">
							<table class='table table-bordered'>
								<tr>
									<th>No</th>
									<th>Engine</th>
									<th>Table</th>
									<th>Rows</th>
									<th>Size</th>
								</tr>
								<?php
								$no = 1;
								foreach($dataTable as $key => $value)
								{
									echo "<tr><td>" . $no . "</td><td>" . $value["Engine"] . "</td><td>" . $value["Table"] . "</td><td>" . $value["Rows"] . "</td><td>" . $value["Size"] . "</td></tr>";
									$no++;
								}
								?>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.box-body -->
			<div class="box-footer clearfix">

			</div>
		</div>
	</div>
</div>