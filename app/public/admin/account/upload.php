<?php

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	$upload = false;
	$allow_ext = array('csv');
	$allow_size = 5000000; // 5 MB
	
	$callback = array(
		"success" => false,
		"messages" => "Data Upload Success"
	);
	
	$file_desc = $_FILES['form_data']['error'];            //file error
	$file_type = $_FILES['form_data']['type'];             //file type
	$file_name = $_FILES['form_data']['name'];             //file name
	$file_size = $_FILES['form_data']['size'];             //file size
	$file_temp = $_FILES['form_data']['tmp_name'];         //file temp
	$file_ext  = pathinfo($file_name, PATHINFO_EXTENSION); //file ext
	
	if(in_array(strtolower($file_ext), $allow_ext))
	{
		if(!$file_size <= $allow_size)
		{
			$upload = true;
		}
		else
		{
			$upload = false;
			$callback["message"] = "File size more then 5 MB";
		}
	}
	else
	{
		$upload = false;
		$callback["message"] = "File extension must .csv";
	}
	
	if ($upload === true)
	{
		$flag = true;
		$data = array();
		
		$filecsv = fopen($file_temp, "r");
		$fieldcsv = fgetcsv($filecsv, 1024, ",", "'");
        
		do
		{
			// Remove First Line
			if ($flag)
			{
				$flag = false;
				continue;
			}
			
			if ($fieldcsv[0])
			{
				$data[] = $fieldcsv;
			}
		}
		while
		(
			($fieldcsv = fgetcsv($filecsv, 1024, ",", "'")) !== false
		);
		
		$callback["success"] = true;
		$callback["data"] = $data;
	}
	else
	{
		$callback["success"] = false;
	}
	
	header("Content-Type: text/html");
	echo json_encode($callback);
	exit;
}

$js = array(
	"plugins/jquery-form/jquery.form.min.js"
);

$jqueryReady = <<<EOF

var jaxsub = {
	target          : '#output',     // target element(s) to be updated with server response 
	beforeSubmit    : beforeSubmit,  // pre-submit callback 
	uploadProgress  : OnProgress,    // progress-bar
	success         : afterSuccess,  // post-submit callback 
	resetForm       : true        	 // reset the form after successful submit
};

$('#submit_data').click(function(e) {
	e.preventDefault();
	$('#form_upload').ajaxSubmit(jaxsub);
});

$('#form_data').on('change', function(){
	//Progress bar
	$('#progressbar').width('0%');
	$('#statustxt').html('0 %');
	$('#statustxt').css('color','#000');
});

function afterSuccess(responseText, statusText, xhr, form) {
	output = $('#output').html();
	json = $.parseJSON(output);
	$.each(json.data, function(k, v) {
		$('#table_upload').append(
			'<tr>' +
				'<td>' + v[0] + '</td>' +
				'<td>' + v[1] + '</td>' +
				'<td>' + v[2] + '</td>' +
				'<td>' + v[3] + '</td>' +
				'<td>' + v[4] + '</td>' +
				'<td>' + v[5] + '</td>' +
				'<td>' + v[6] + '</td>' +
				'<td>' + v[7] + '</td>' +
			'</tr>'
		);
	});
}

function beforeSubmit() {
	
	if (window.File && window.FileReader && window.FileList && window.Blob) {
		
		if (!$('#form_data').val()) {
			$("#output").html("Please select file");
			return false
		}
		
		var file_size = $('#form_data')[0].files[0].size;
		var file_name = $('#form_data')[0].files[0].name;
		var file_ext = file_name.split('.').pop().toLowerCase();
		
		if ($.inArray(file_ext, ['csv']) == -1) {
			alert("File harus CSV silahkan lakukan [save as] pilih type CSV (Comma Delimited)(*.csv)")
			return false;
		}
				
		if (file_size > 5000000) {
			$("#output").html("<b>File to large " + bytesToSize(file_size) + "</b> Failed to upload more than 5 MB");
			return false;
		}
		
		//Progress bar
		$('#progressbar').width('0%');
		$('#statustxt').html('0 %');
		$('#statustxt').css('color','#000');
	}
}

function bytesToSize(bytes) {
	if (bytes == 0) return '0 Bytes';
	sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function OnProgress(event, position, total, percentComplete) {
	$('#progressbar').width(percentComplete + '%');
	$('#statustxt').html(percentComplete + ' %');
	if (percentComplete > 50) $('#statustxt').css('color','#fff');
}

EOF;
?>
<style type="text/css">
#progressbox {
	border:1px solid #b4b4b4;
	padding:1px;
	position:relative;
	width:200px;
	border-radius:3px;
	margin:10px 0;
	text-align:left
}
#progressbar {
	height:20px;
	border-radius:3px;
	background-color:#b4b4b4;
	width:0%
}
#statustxt {
	left: 0; 
	right: 0; 
	top: 1px;
	width: 50px;
	margin-left: auto; 
	margin-right: auto; 
	text-align: center;
	position: absolute; 
}
</style>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Upload</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				
				<div class="callout callout-success">
					<h4>Upload File</h4>
					<p>File type .csv</p>
				</div>

				<form id="form_upload" action="<?=$thisurl?>" accept-charset="UTF-8" onSubmit="return false" method="post" enctype="multipart/form-data">							
					<div class="form-group">
						<input id="form_data" name="form_data" type="file" accept=".csv"/>
						<div id="progressbox">									
							<div id="progressbar"></div>
							<div id="statustxt">0 %</div>
						</div>
						<button id="submit_data" name="submit_data" type="submit">Upload</button>
					</div>
				</form>
				
				<div class="callout callout-default">
					<h4>Output JSON</h4>
					<div id="output"></div>
				</div>
				
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-condensed dataTable no-footer">
						<thead>
							<th>login_user_id</th>
							<th>login_level_id</th>
							<th>login_status</th>
							<th>login_username</th>
							<th>login_password</th>
							<th>login_name</th>
							<th>login_email</th>
							<th>login_image</th>
						</thead>
						<tbody id="table_upload"></tbody>
					</table>
				</div>
				
			</div>
			<!-- /.box-body -->
		</div>
	  <!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->