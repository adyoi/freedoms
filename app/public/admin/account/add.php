<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

$listpath = Routes::changeurl('list');
$addpath = Routes::changeurl('add');
$editpath = Routes::changeurl('edit');
$deletepath = Routes::changeurl('delete');

if(array_key_exists("action", $_POST) && array_key_exists("action", $_POST) == 'save')
{
	$callback = array(
		
		"success" => false,
		"messages" => "Data Saved Success"
	
	);
	
	$database = Database_mysqli :: opendb( Profile :: data_db("admin") );

	if(!$database == null)
	{
		$login_user_id = $database->real_escape_string($_POST['plevel']);
		$login_level_id = $database->real_escape_string($_POST['level']);
		$login_status = $database->real_escape_string(isset($_POST['status']) == "1" ? 1 : 0);
		$login_username = $database->real_escape_string($_POST['username']);
		$login_password = $database->real_escape_string($_POST['cpassword']);
		$login_name = $database->real_escape_string($_POST['name']);
		$login_email = $database->real_escape_string($_POST['email']);
		$login_image = $database->real_escape_string($_POST['image']);
		
		$sql = sprintf("SELECT login_username, login_email FROM freedoms_login WHERE login_username = '%s' OR login_email = '%s'", $login_username, $login_email);
		
		if ( ($data = Database_mysqli::query($sql)) !== false )
		{
			if ( count($data) > 0 )
			{
				$callback["messages"] = "Username or Email Duplicated";
			}
			else
			{
				$sql = sprintf("INSERT INTO `freedoms_login` (
							login_user_id,
							login_level_id,
							login_status,
							login_username,
							login_password,
							login_name,
							login_email,
							login_image
						) VALUES ( %d, %d, %d, '%s', '%s', '%s', '%s', '%s' )", 
							$login_user_id,
							$login_level_id,
							$login_status,
							$login_username,
							md5($login_password),
							$login_name,
							$login_email,
							$login_image
				);
				
				if ( ($data = Database_mysqli::update($sql)) === true )
				{
					$callback["success"] = true;
				}
				else
				{
					$callback["messages"] = "Database Query Error : " . $database->error;	
				}
				
			}
		}
		else
		{
			$callback["messages"] = "Database Query Error : " . $database->error;
		}
		
		Database_mysqli::closedb();
		
	}
	
	header("Content-Type: application/json");
	echo json_encode($callback);
	exit;
	
}

$database = Database_mysqli :: opendb( Profile :: data_db("admin") );

if(!$database == null)
{
	$sql = "SELECT login_level_id, login_level_name from freedoms_login_level";
					
	$dataLevel = Database_mysqli::query($sql);
	
	$sql = "SELECT login_id, login_username from freedoms_login  ";
					
	$dataParentLevel = Database_mysqli::query($sql);
	
	Database_mysqli :: closedb();
}

$css = array();

$js = array(
	"plugins/jquery-validation/jquery.validate.min.js",
	"plugins/notify/notify.js"
);

$jqueryReady = <<<EOF

$('#name').focus();

$("#submit").validate({
    rules: {
		name: {required: true},
		email: {required: true, email: true},
		username: {required: true, minlength: 4},
		password: {required: true, minlength: 4},
		cpassword: {required: true, minlength: 4, equalTo: "#password"},
		level: {required: true},
		plevel: {required: true},
    },
    submitHandler: function(form) {
		$.ajax({
			url: "{$thisurl}",
			type: "post",
			data: $(form).serialize(),
			success: function (response) {
				
				if(response.success == true){

					$(form).trigger('reset');
					Notify(response.messages, null, null, 'success');
					$('#name').focus();
					
				}else{
					
					Notify(response.messages, null, null, 'danger');
				}

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
    },
	highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
		if (element.parent('.input-group').length) { 
			error.insertAfter(element.parent());
		} else if (element.hasClass('select2')) {     
			error.insertAfter(element.next('span'));
		} else {                                      
			error.insertAfter(element);
		}
    }
});

$('#cancel').on( 'click', function (e){
	e.preventDefault();
	window.location = "{$listpath}";
});

EOF;
?>
<style>
#notifications {
    cursor: pointer;
    position: fixed;
    right: 0px;
    z-index: 9999;
    bottom: 0px;
    margin-bottom: 42px;
    margin-right: 55px;
    max-width: 300px;   
}

.form-group.has-error .input-group-addon{
	
	border-color: #dd4b39;
    box-shadow: none;
}

.form-group.has-error .select2-selection {
	
	border-color: #dd4b39;
    box-shadow: none;
}

.select2-container--default .select2-selection--single {
	height: 34px;
    border-radius: 0px;
	
}

.select2-container--default .select2-selection--single .select2-selection__arrow {
	height: 34px;
	
}
.required {
	
	color: #dd4b39;
}
</style>

<div id="notifications"></div>

<div class="row">
	 <div class="col-md-12">
	  <!-- Horizontal Form -->
	  <div class="box box-info">
		<div class="box-header with-border">
		  <h3 class="box-title">Add New Account</h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" id="submit">
		  <div class="box-body">
			<div class="form-group">
			  <label for="name" class="col-sm-3 control-label">Name <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="text" class="form-control" id="name" name="name" placeholder="Name">
			  </div>
			</div>
			<div class="form-group">
			  <label for="email" class="col-sm-3 control-label">Email <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="text" class="form-control" id="email" name="email" placeholder="Email">
			  </div>
			</div>
			<div class="form-group">
			  <label for="image" class="col-sm-3 control-label">Image </label>
			  <div class="col-sm-4">
				<input type="text" class="form-control" id="image" name="image" placeholder="image.png">
			  </div>
			</div>
			<div class="form-group">
			  <label for="username" class="col-sm-3 control-label">Username <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="text" class="form-control" id="username" name="username" placeholder="Username">
			  </div>
			</div>
			<div class="form-group">
			  <label for="password" class="col-sm-3 control-label">Password <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
			  </div>
			</div>
			<div class="form-group">
			  <label for="cpassword" class="col-sm-3 control-label">Confirm Password <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password">
			  </div>
			</div>
			<div class="form-group">
			  <label for="plevel" class="col-sm-3 control-label">Parent Level <span class="required">*</span></label>
			  <div class="col-sm-4">
				<select class="form-control" id="plevel" name="plevel">
					<option value="">Please Select</option>
				<?php
					foreach($dataParentLevel as $value)
					{
						echo '<option value="' . $value["login_id"] . '">' . $value["login_username"] . '</option>';
					}
				?>
				</select>
			  </div>
			</div>
			<div class="form-group">
			  <label for="level" class="col-sm-3 control-label">Level <span class="required">*</span></label>
			  <div class="col-sm-4">
				<select class="form-control" id="level" name="level">
					<option value="">Please Select</option>
				<?php
					foreach($dataLevel as $value)
					{
						echo '<option value="' . $value["login_level_id"] . '">' . $value["login_level_name"] . '</option>';
					}
				?>
				</select>
			  </div>
			</div>
			<div class="form-group">
			  <label for="status" class="col-sm-3 control-label">Status </label>
			  <div class="col-sm-4">
				<label class="control-label"><input type="checkbox" id="status" name="status" checked="checked" value="1" /> Yes</label>
			  </div>
			</div>
			<div class="form-group">
			  <label for="inputPassword3" class="col-sm-3 control-label"></label>
			  <div class="col-sm-4">
				<button type="submit" name="action" value="save" class="btn btn-success">Save</button>
				<button id="cancel" class="btn btn-default">Cancel</button>
			  </div>
			</div>
		  </div>
		  <!-- /.box-body -->
		  <div class="box-footer">
			
		  </div>
		  <!-- /.box-footer -->
		</form>
	  </div>
	  <!-- /.box -->
	</div>
</div>