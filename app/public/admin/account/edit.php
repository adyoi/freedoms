<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

$listpath = Routes::changeurl('list');
$addpath = Routes::changeurl('add');
$editpath = Routes::changeurl('edit');
$deletepath = Routes::changeurl('delete');

$id = 0;

if(array_key_exists(3, $thispath) && is_numeric($thispath[3]))
{
	$id = intval($thispath[3]);
}

if(array_key_exists("action", $_GET) && array_key_exists("action", $_GET) == 'get')
{
	$callback = array(
		
		"success" => false,
		"messages" => "Data Selected"
	
	);
	
	$database = Database_mysqli::opendb( Profile::data_db("admin") );

	if(!$database == null)
	{
		$id = $database->real_escape_string($id);
		
		$sql = sprintf(
				
			"SELECT * FROM freedoms_login WHERE login_id = %d AND login_username not in('admin')", $id
		);
					
		if ( ($data = Database_mysqli::query($sql)) !== false )
		{
			if(count($data) > 0)
			{
				$callback["success"] = true;			
				$callback["data"] = $data;			
			}
			else
			{
				$callback["messages"] = "No data Selected";	
			}
		}
		else
		{
			$callback["messages"] = "Database Query Error : " . $database->error;
		}

		Database_mysqli::closedb();
		
	}
	
	header("Content-Type: application/json");
	echo json_encode($callback);
	exit;
}

if(array_key_exists("action", $_POST) && array_key_exists("action", $_POST) == 'edit')
{
	$callback = array(
		
		"success" => false,
		"messages" => "Data Edited Success"
	
	);
	
	$database = Database_mysqli::opendb( Profile::data_db("admin") );

	if(!$database == null)
	{
		$login_user_id = $database->real_escape_string($_POST['plevel']);
		$login_level_id = $database->real_escape_string($_POST['level']);
		$login_status = $database->real_escape_string(isset($_POST['status']) == "1" ? 1 : 0);
		$login_username = $database->real_escape_string($_POST['username']);
		$login_password = $database->real_escape_string($_POST['cpassword']);
		$login_name = $database->real_escape_string($_POST['name']);
		$login_email = $database->real_escape_string($_POST['email']);
		$login_image = $database->real_escape_string($_POST['image']);
		
		$sql = sprintf(
				
			"UPDATE freedoms_login
				SET 
					login_user_id = %d,
					login_level_id = %d,
					login_status = '%s',
					login_username = '%s',
					login_password = '%s',
					login_name = '%s',
					login_email = '%s',
					login_image = '%s'
				WHERE
					login_id = %d 
				AND
					login_username not in('admin')
			", 
				$login_user_id,
				$login_level_id,
				$login_status,
				$login_username,
				md5($login_password),
				$login_name,
				$login_email,
				$login_image,
				$id
		);
		
		if ( ($data = Database_mysqli::update($sql)) != false )
		{	
			$callback["success"] = true;
		}
		else
		{
			$callback["messages"] = $database->error;	
		}

		Database_mysqli::closedb();
		
	}
	
	header("Content-Type: application/json");
	echo json_encode($callback);
	exit;
	
}

$database = Database_mysqli :: opendb( Profile :: data_db("admin") );

if(!$database == null)
{
	$sql = "SELECT login_level_id, login_level_name from freedoms_login_level";
					
	$dataLevel = Database_mysqli::query($sql);
	
	$sql = "SELECT login_id, login_username from freedoms_login  ";
					
	$dataParentLevel = Database_mysqli::query($sql);
	
	Database_mysqli :: closedb();
}

$css = array();

$js = array(
	"plugins/jquery-validation/jquery.validate.min.js",
	"plugins/notify/notify.js"
);

$jqueryReady = <<<EOF

$.ajax({
	url: "{$thisurl}",
	type: "get",
	data: {
		action: 'get'
	},
	success: function (response) {
		
		if(response.success == true) {
			
			if(response.data.length > 0){

				$.each(response.data[0], function(k,v) {
					
					$('#'+k).val(v);
					
				});

				$('#login_status').prop("checked", (response.data[0].login_status == 1 ? true : false));
				$('#login_status').val(response.data[0].login_status);
				
				Notify(response.messages, null, null, 'success');
				
			}else{
				
				Notify("Data Invalid", null, null, 'warning');
			}

		}else{
			
			Notify(response.messages, null, null, 'danger');
		}

	},
	error: function(jqXHR, textStatus, errorThrown) {
		console.log(textStatus, errorThrown);
	}
});


$("#submit").validate({
    rules: {
		name: {required: true},
		email: {required: true, email: true},
		username: {required: true, minlength: 4},
		password: {required: true, minlength: 4},
		cpassword: {required: true, minlength: 4, equalTo: "#password"},
		level: {required: true},
		plevel: {required: true},
    },
    submitHandler: function(form) {
		$.ajax({
			url: "{$thisurl}",
			type: "post",
			data: $(form).serialize(),
			success: function (response) {
				
				if(response.success == true){

					Notify(response.messages, null, null, 'success');
					window.location = "{$listpath}";
					
				}else{
					
					Notify(response.messages, null, null, 'danger');
				}

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
    },
	highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
		if (element.parent('.input-group').length) { 
			error.insertAfter(element.parent());
		} else if (element.hasClass('select2')) {     
			error.insertAfter(element.next('span'));
		} else {                                      
			error.insertAfter(element);
		}
    }
});

$('#cancel').on( 'click', function (e){
	e.preventDefault();
	window.location = "{$listpath}";
});

EOF;
?>
<style>
#notifications {
    cursor: pointer;
    position: fixed;
    right: 0px;
    z-index: 9999;
    bottom: 0px;
    margin-bottom: 42px;
    margin-right: 55px;
    max-width: 300px;   
}

.form-group.has-error .input-group-addon{
	
	border-color: #dd4b39;
    box-shadow: none;
}

.form-group.has-error .select2-selection {
	
	border-color: #dd4b39;
    box-shadow: none;
}

.select2-container--default .select2-selection--single {
	height: 34px;
    border-radius: 0px;
	
}

.select2-container--default .select2-selection--single .select2-selection__arrow {
	height: 34px;
	
}
.required {
	
	color: #dd4b39;
}
</style>

<div id="notifications"></div>

<div class="row">
	 <div class="col-md-12">
	  <!-- Horizontal Form -->
	  <div class="box box-info">
		<div class="box-header with-border">
		  <h3 class="box-title">Trace Website</h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" id="submit">
		  <div class="box-body">
			<div class="form-group">
			  <label for="name" class="col-sm-3 control-label">Name <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="text" class="form-control" id="login_name" name="name" placeholder="Name">
			  </div>
			</div>
			<div class="form-group">
			  <label for="email" class="col-sm-3 control-label">Email <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="text" class="form-control" id="login_email" name="email" placeholder="Email">
			  </div>
			</div>
			<div class="form-group">
			  <label for="image" class="col-sm-3 control-label">Image </label>
			  <div class="col-sm-4">
				<input type="text" class="form-control" id="login_image" name="image" placeholder="image.png">
			  </div>
			</div>
			<div class="form-group">
			  <label for="username" class="col-sm-3 control-label">Username <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="text" class="form-control" id="login_username" name="username" placeholder="Username">
			  </div>
			</div>
			<div class="form-group">
			  <label for="password" class="col-sm-3 control-label">Password <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
			  </div>
			</div>
			<div class="form-group">
			  <label for="cpassword" class="col-sm-3 control-label">Confirm Password <span class="required">*</span></label>
			  <div class="col-sm-4">
				<input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password">
			  </div>
			</div>
			<div class="form-group">
			  <label for="plevel" class="col-sm-3 control-label">Parent Level <span class="required">*</span></label>
			  <div class="col-sm-4">
				<select class="form-control" id="login_user_id" name="plevel">
					<option value="">Please Select</option>
				<?php
					foreach($dataParentLevel as $value)
					{
						echo '<option value="' . $value["login_id"] . '">' . $value["login_username"] . '</option>';
					}
				?>
				</select>
			  </div>
			</div>
			<div class="form-group">
			  <label for="level" class="col-sm-3 control-label">Level <span class="required">*</span></label>
			  <div class="col-sm-4">
				<select class="form-control" id="login_level_id" name="level">
					<option value="">Please Select</option>
				<?php
					foreach($dataLevel as $value)
					{
						echo '<option value="' . $value["login_level_id"] . '">' . $value["login_level_name"] . '</option>';
					}
				?>
				</select>
			  </div>
			</div>
			<div class="form-group">
			  <label for="status" class="col-sm-3 control-label">Status </label>
			  <div class="col-sm-4">
				<label class="control-label"><input type="checkbox" id="login_status" name="status" value="1" /> Yes</label>
			  </div>
			</div>
			<div class="form-group">
			  <label for="inputPassword3" class="col-sm-2 control-label"></label>
			  <div class="col-sm-4">
				<button type="submit" name="action" value="edit" class="btn btn-warning">Edit</button>
				<button id="cancel" class="btn btn-default">Cancel</button>
			  </div>
			</div>
		  </div>
		  <!-- /.box-body -->
		  <div class="box-footer">
			
		  </div>
		  <!-- /.box-footer -->
		</form>
	  </div>
	  <!-- /.box -->
	</div>
</div>