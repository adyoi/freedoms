<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

$addpath = Routes :: changeurl('add');
$editpath = Routes :: changeurl('edit');
$deletepath = Routes :: changeurl('delete');
$uploadpath = Routes :: changeurl('upload');

$idprofile = Profile :: data_profile("id");
$iduserprofile = Profile :: data_profile("id_user");
$levelprofile = Profile :: data_profile("level");

$columns = array(
	
	array(
		"field" => "login_id",
		"title" => "id",
		"display" => false,
		"order" => true,
		"search" => false
	),
	array(
		"field" => "''",
		"alias" => "checkbox",
		"title" => "<i class='fa  fa-circle-o'></i>",
		"display" => true,
		"order" => false,
		"search" => false,
		"filter" => array(
			"type" => "check"
		),
		"render" => <<<EOF
			function ( data, type, row ) {
				
				return '<input type="checkbox" class="select-checkbox" value="' + row.login_id + '">';
			}
EOF
	),
	array(
		"field" => "login_username",
		"title" => "<i class='fa fa-user' style='margin-right:5px'></i> Username",
		"display" => true,
		"order" => true,
		"search" => true,
		"filter" => array(
			"type" => "text"
		)
	),
	array(
		"field" => "login_email",
		"title" => "<i class='fa fa-envelope-o' style='margin-right:5px'></i> Email",
		"display" => true,
		"order" => true,
		"search" => true,
		"filter" => array(
			"type" => "text",
			"width" => 100
		)
	),
	array(
		"field" => "login_level_id",
		"title" => "<i class='fa fa-star' style='margin-right:5px'></i> Level",
		"display" => true,
		"order" => true,
		"search" => true,
		"filter" => array(
			"type" => "select",
			"width" => 100,
			"option" => array(
				"Super Administrator" => 1,
				"Administrator" => 2,
				"Moderator" => 3,
				"User" => 4
			)
		),
		"render" => <<<EOF

				function ( data, type, row ) {
					
					if (row.login_level_id == 1)
						return "Super Administrator";
					else if (row.login_level_id == 2)
						return "Administrator";
					else if (row.login_level_id == 3)
						return "Moderator";
					else if (row.login_level_id == 4)
						return "User";
					else
						return "-";
				}
EOF
	),
	array(
		"field" => "login_status",
		"title" => "<i class='fa fa-check-square-o' style='margin-right:5px'></i> Status",
		"display" => true,
		"order" => true,
		"search" => true,
		"filter" => array(
			"type" => "select",
			"width" => 10,
			"option" => array(
				"Yes" => 1,
				"No" => 0
			)
		),
		"render" => <<<EOF

				function ( data, type, row ) {
					
					if (row.login_status == 1)
						return "Yes";
					else
						return "No";
				}
EOF
	),
	array(
		"field" => "''",
		"alias" => "action",
		"title" => "<i class='fa fa-gear' style='margin-right:5px'></i> Action",
		"display" => true,
		"order" => false,
		"search" => true,
		"filter" => array(
			"type" => "action",
			"width" => "150px"
		),
		"render" => <<<EOF

				function ( data, type, row ) {
					return "<div style='text-align: center'>" +
								"<a href='{$editpath}" + row.login_id + "' class='edit btn btn-xs btn-warning'>" +
									"<i class='fa fa-edit'></i> Edit" +
								"</a> " +
								"<a href='#' class='delete_msg btn btn-xs btn-danger' data-id='" + row.login_id + "' data-title='" + row.login_username + "' data-toggle='modal' data-target='#delete'>" +
									"<i class='fa fa-close'></i> Delete" +
								"</a>" + 
							"</div>";
				}
EOF
	)

);

$columnDT = Datatable :: buildColumn($columns);
$tableDT =  Datatable :: buildTable('example', $columns);

if(array_key_exists("get", $_GET))
{
	$select = Datatable :: sqlSelect($columns);
	$search = Datatable :: sqlSearch($columns);
	$filter = Datatable :: sqlFilter($columns);

	$database = Database_mysqli :: opendb( Profile :: data_db("admin") );

	$recordsTotal = 0;
	$recordsFiltered = 0;
	$datatable = array();

	if(!$database == null)
	{
		$sql = "SELECT " . $select . " FROM freedoms_login";
		$data = Database_mysqli :: query($sql);
		$recordsTotal = count($data);
		
		$sql = "SELECT " . $select . " FROM freedoms_login " . $search;
		$data = Database_mysqli :: query($sql);
		$recordsFiltered = count($data);

		$sql = "SELECT " . $select . " FROM freedoms_login " . $search . $filter;
		$data = Database_mysqli :: query($sql);
		
		// die($sql);
		
		if(count($data) > 0)
		{
			foreach($data as $val)
			{
				$datatable[] = $val;
			}
		}

		Database_mysqli :: closedb();	
	}

	header("Content-Type: application/json");
	echo json_encode(
		array(
			"draw" => isset($_GET['draw']) ? intval($_GET['draw']) : intval(1), 
			"recordsTotal" => $recordsTotal, 
			"recordsFiltered" => $recordsFiltered, 
			"data" => $datatable
		)
	);
	exit;
}

$css = array(
	"plugins/datatables/dataTables.bootstrap.css",
	"plugins/datatables/extensions/Select/css/select.dataTables.min.css",
	"plugins/datatables/extensions/ColVis/css/dataTables.colVis.min.css",
	"plugins/datatables/extensions/Buttons/css/buttons.dataTables.min.css",
	"plugins/datatables/extensions/Buttons/css/buttons.bootstrap.min.css"
);

$js = array(
	"plugins/datatables/jquery.dataTables.min.js",
	"plugins/datatables/dataTables.bootstrap.min.js",
	"plugins/datatables/extensions/Select/js/dataTables.select.min.js",
	"plugins/datatables/extensions/ColVis/js/dataTables.colVis.min.js",
	"plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js",
	"plugins/datatables/extensions/Buttons/js/buttons.bootstrap.min.js",
	"plugins/datatables/extensions/Buttons/js/buttons.print.min.js",
	"plugins/datatables/extensions/Buttons/js/buttons.flash.min.js",
	"plugins/datatables/extensions/Buttons/js/buttons.html5.min.js"
);

$jqueryReady = <<<EOF
var editor;
var rows_selected = [];
var table = $('#example').DataTable({
	ajax: "{$thisurl}?get",
	processing: true,
	serverSide: true,
	orderCellsTop: true,
    autoWidth: false,
	lengthMenu: [
		[15, 25, 50, 100, 500, 18446744073709551615], 
		[15, 25, 50, 100, 500, 'ALL']
	],
	dom: 
		"<'row'<'col-sm-2'l><'col-sm-2'C><'col-sm-4'B><'col-sm-4 text-align-right'i>>" +
	    "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'p>>",
	columns: {$columnDT},
	buttons: [
		{
			extend: 'copy',
			text: '<i class="fa fa-copy"></i> Copy',
			title: '',
			exportOptions: {
				columns: ':visible'
			}
		}, 
		{
			extend: 'print',
			text: '<i class="fa fa-print"></i> Print',
			title: 'Web Tracing',
			message: '<i>This data is <u>secret</u></i>',
			header: true,
			footer: false,
			exportOptions: {
				columns: ':visible'
			}
		}, 
		{
			extend: 'excel',
			text: '<i class="fa fa-file-excel-o"></i> Excel',
			title: '',
			exportOptions: {
				columns: ':visible'
			}
		}
    ]
});

$(".searching").on( 'click', function () {
	table.columns().search('');
	var col = 0;
	table.columns().every( function () {
		col++;
	});
	var fil = 0;
	$('.filter').each(function() { 
		fil++;
	});
	var addcol = col - fil;
	$('.filter').each(function(i) { 
		if(this.value.length) {
			operator = $(this).parent('div').prev('div').find('.operator').val();
			table.column(i + addcol).search(operator + "|" + this.value);
		}
	});
	table.draw();
});

$(".resetting").on( 'click', function () {
	$('.filter').val(''); $('.operator').val('=');
	table.columns().search('').draw();
});

$('#example tbody').on('click', 'tr', function () {
	$(this).toggleClass('selected');
	ipt = $(this).find('.select-checkbox');
	if (ipt.is(":checked"))
		ipt.prop('checked', false);
	else
		ipt.prop('checked', true);
});

$('.select-all').on('click', function () {
	$('#example tbody tr').find('input').prop('checked', this.checked);
	if (this.checked)
		$('#example tbody tr').addClass('selected');
	else
		$('#example tbody tr').removeClass('selected');
});

$('#example tbody').on('click', '.select-checkbox', function () {  
	if ($(this).is(":checked"))
		$(this).prop('checked', false);
	else
		$(this).prop('checked', true);
});

$('a.add').on( 'click', function () {
	window.location = "{$addpath}";
});

$('a.upload').on( 'click', function () {
	window.location = "{$uploadpath}";
});

$('a.action').on('click', function () {
	var asu = [];
	$('#example tbody tr').each(function(i) {
		var ipt = $(this).find('input');
		if (ipt.is(":checked")) {
			asu.push(ipt.val());
		}
	});
	console.log(asu);
});

 /* Delete Modal 1 */
$(document).on('click', '.delete_msg', function() {
	var this_href = $(this).data('toggle');
	alert(this_href);
	var modal_title = $('.modal-title').text();
	$('.modal-title').text(modal_title + ' inject success');
});

/* Delete Modal 2 */
$('#delete').on('show.bs.modal', function(e) {
	var this_href = $(e.relatedTarget).data('toggle');
	alert(this_href);
	var modal_body = $(e.currentTarget).find('.modal-body').text();
	$(e.currentTarget).find('.modal-body').text(modal_body + ' inject success');
});

EOF;
?>

<style>
table.dataTable thead > tr > th {
	padding-right:9px;
}
.text-align-right {
	text-align:right;
}
.table-wrapper {
	overflow:scroll;
	overflow-y:hidden;
	margin:10px;
}
.dataTables_wrapper {
	min-width: 1230px;
    margin: 20px;
}
div.ColVis {
	float: none;
    display: inline-block;
    margin: auto;
}
.filter {
    border: 1px solid #ddd;
	padding: 2px 5px;
	outline: none;
}
.buttons-copy, .buttons-print, .buttons-excel {
	background: #fff
}
.fleft {
	float:left;
	width:80%
}
.fright {
	float:left;
	width:20%
}
.fleft input{
	width:100%;
	border-left:none
}
.fleft select{
	width:100%;
	border-left: none;
}
.fright select{
	width:100%;
	height: 26px;
	border: 1px solid #ddd;
	outline: none
}
</style>

<!-- Modal Delete -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="delete_modlabel" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="delete_modlabel">Delete Data Account</h4>
			</div>
			<div class="modal-body">
				Are you sure want to delete this data ?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger delete">Delete</button>
				<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Data Account</h3>
				<div class="pull-right">
					<a class="action btn btn-xs btn-warning"><i class="fa fa-edit"></i> Action</a>
					<a class="upload btn btn-xs btn-info"><i class="fa fa-upload"></i> Upload</a>
					<a class="add btn btn-xs btn-success"><i class="fa fa-plus"></i> Add New</a>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<?php echo($tableDT); ?>
				</div>
			</div>
			<!-- /.box-body -->
		</div>
	  <!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->