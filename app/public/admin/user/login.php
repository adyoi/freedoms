<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

?>
<form action="<?php echo $baseurl; ?>/admin/signin" method="post">
  <div class="form-group has-feedback">
	<input type="text" name="username" class="form-control" placeholder="Username" required >
	<span class="glyphicon glyphicon-user form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
	<input type="password" name="password" class="form-control" placeholder="Password" required >
	<span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="row">
	<div class="col-xs-8">
	  <!-- div class="checkbox icheck">
		<label>
		  <input type="checkbox"> Remember Me
		</label>
	  </div -->
	</div>
	<!-- /.col -->
	<div class="col-xs-4">
	  <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
	</div>
	<!-- /.col -->
  </div>
</form>