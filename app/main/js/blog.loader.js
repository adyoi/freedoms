/* Javascript Script Loader v1.0 By Adi Apriyanto*/

baseurl = window.location.protocol + '//' + window.location.hostname + window.location.pathname;

window.onload = new function (){
appendCSS(baseurl + 'css/jquery-ui.css');
appendJSS(baseurl + 'js/jquery.min.js');
appendJSS(baseurl + 'js/jquery-ui.min.js');
appendJSS(baseurl + 'js/jquery.tablesorter.js');
appendJSS(baseurl + 'js/jquery.tablesorter.widgets.js');
appendJSS(baseurl + 'js/excel.js');
appendJSS(baseurl + 'js/main.js');
};

function appendJSS(fileJS){
head = document.getElementsByTagName('head')[0];
js  = document.createElement('script');
js.type = 'text/javascript';
js.src  = fileJS;
js.async = false;
script = head.appendChild(js);
script.onload = function(){return true}
};

function appendCSS(fileCSS){
head = document.getElementsByTagName('head')[0];
css = document.createElement('link');
css.rel = 'stylesheet';
css.type = 'text/css';
css.href = fileCSS;
script = head.appendChild(css);
script.onload = function(){return true}
};