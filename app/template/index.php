<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

$start = microtime(TRUE);

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Freedoms is Simply and Powerful PHP Class">
	<meta name="author" content="adyoi">
	<link rel="icon" href="<?=Routes :: baseurl()?>/app/assets/main/favicon.ico">
	<title>Freedoms PHP Class</title>
	<link href="<?=Routes :: baseurl()?>/app/assets/main/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=Routes :: baseurl()?>/app/assets/main/css/starter-template.css" rel="stylesheet">
	<?php if (is_array($css) && count($css) > 0):?>
	<?php foreach($css as $key):?>
	<?="<link rel='stylesheet' href='" . Routes :: baseurl() . "/app/assets/" .  $key . "'>" . PHP_EOL?>
	<?php endforeach?>
	<?php endif?>
	<!--[if lt IE 9]>
		<script src="<?=Routes :: baseurl()?>/app/assets/main/js/ie8-responsive-file-warning.js"></script>
	<![endif]-->
	<script src="<?=Routes :: baseurl()?>/app/assets/main/js/ie-emulation-modes-warning.js"></script>
	<!--[if lt IE 9]>
		<script src="<?=Routes :: baseurl()?>/app/assets/main/js/html5shiv.min.js"></script>
		<script src="<?=Routes :: baseurl()?>/app/assets/main/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
	<div class="navbar-header">
	  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
	  <a class="navbar-brand" href="<?=Routes :: baseurl()?>/">Freedoms</a>
	</div>
	<div class="collapse navbar-collapse">
	  <ul class="nav navbar-nav">
		<li><a href="<?=Routes :: baseurl()?>/">Home</a></li>
		<li><a href="<?=Routes :: baseurl()?>/document">Document</a></li>
		<li><a href="<?=Routes :: baseurl()?>/about">About</a></li>
		<li><?php if(Session::get('loggedIn') == true):?>
			<a href="<?=Routes :: baseurl()?>/index/signout">Example Logout</a>
			<?php else:?>
			<a href="<?=Routes :: baseurl()?>/index/login">Example Login</a>
			<?php endif?></li>
		<li><a href="<?=Routes :: baseurl()?>/admin/">Admin Backend</a></li>
	  </ul>
	</div>
  </div>
</div>

<div class="container">
	<div class="starter-template">
		<?=$template?>
	</div>
</div>

<div style="color:green;text-align:center;padding:0 0 5px">
	Page generated in <?=round((microtime(TRUE) - $start), 2) * 1000?> ms
</div>

<div id="footer" style="text-align:center;padding:5px 0 35px">
	&copy; Freedoms PHP Class 2017
</div>

<script src="<?=Routes :: baseurl()?>/app/assets/main/js/jquery.min.js"></script>
<script src="<?=Routes :: baseurl()?>/app/assets/main/js/bootstrap.min.js"></script>
<script src="<?=Routes :: baseurl()?>/app/assets/main/js/ie10-viewport-bug-workaround.js"></script>
<script src="<?=Routes :: baseurl()?>/app/assets/main/js/custom.js"></script>
<?php if (is_array($js) && count($js) > 0):?>
<?php foreach($js as $key):?>
<?="<script src='" . Routes :: baseurl(). "/app/assets/" . $key . "'></script>" . PHP_EOL?>
<?php endforeach?>
<?php endif?>
<script type="text/javascript">
$(document).ready( function () {
<?=$jqueryReady?>
});
</script>
<script type="text/javascript">
/* Prevent <iframe> */
if (window.top !== window.self) window.top.location.replace(window.self.location.href);
</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-74057494-2', 'auto');
	ga('send', 'pageview');
</script>
</body>
</html>