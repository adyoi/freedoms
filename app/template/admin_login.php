<?php namespace Freedoms; if(!defined('names')) exit('forbidden access');

?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=Config :: $backend["info"]["names"]?> | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Favicon -->
  <link rel="icon" href="<?=Routes :: baseurl()?>/app/assets/main/favicon.ico">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=Routes :: baseurl()?>/app/assets/plugins/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=Routes :: baseurl()?>/app/assets/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=Routes :: baseurl()?>/app/assets/plugins/ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=Routes :: baseurl()?>/app/assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=Routes :: baseurl()?>/app/assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?=Routes :: baseurl()?>/admin"><b><?=Config :: $backend["info"]["names"]?></b> v<?=Config :: $backend["info"]["version"]?></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><?=$status?></p>
	<?=$template?>
    <a href="<?=Routes :: baseurl()?>">Back to Home</a><br>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<!-- jQuery 2.2.3 -->
<script src="<?=Routes :: baseurl()?>/app/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=Routes :: baseurl()?>/app/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?=Routes :: baseurl()?>/app/assets/plugins/iCheck/icheck.min.js"></script>
<!-- backstretch -->
<script src="<?=Routes :: baseurl()?>/app/assets/plugins/backstretch/jquery.backstretch.min.js"></script>
<script type="text/javascript">
/* Prevent <iframe> */
if (window.top !== window.self) window.top.location.replace(window.self.location.href);
$(function () {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});
	$.backstretch([
		  "<?=Routes :: baseurl()?>/app/assets/plugins/backstretch/1.jpg"
		, "<?=Routes :: baseurl()?>/app/assets/plugins/backstretch/2.jpg"
		, "<?=Routes :: baseurl()?>/app/assets/plugins/backstretch/3.jpg"
	], {duration: 5000, fade: 2000});
});
</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-74057494-2', 'auto');
	ga('send', 'pageview');
</script>
</body>
</html>