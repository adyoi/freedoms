<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Rules
	{
		public function __construct()
		{	
			/* GLOBAL CLASS REQUIREMENT */
			AutoLoad :: load('engines\Session');
			AutoLoad :: load('engines\Request');
			AutoLoad :: load('engines\Database_mysqli');
			
			/* SETTINGS REQUIREMENT */
			date_default_timezone_set( Config :: $config['options']['timeszone'] );
		}
	}
	if(!defined('names')) exit('forbidden access');
}