<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	class Config
	{
		/* DEFAULT CONFIGURATION */
		
		public static $db = array(

			"admin" => array(

				"type" => "mysql", // PDO only
				"host" => "mysql.idhostinger.com",
				"user" => "u594771431_free",
				"pass" => "3dalc6dblodsp10so2",
				"dbas" => "u594771431_free"
			)
		);
		
		public static $config = array(
			
			"app" => array(
			
				"ext" => ".php",
				"path" => "./app/public/"
			),
			
			"session" => array(
			
				"name" => "Freedoms",
				
			),
			
			"controller" => array(
			
				"name" => "index",
				"path" => "./app/public/"
			),
			
			"view" => array(
			
				"path" => "./app/public/index/"
			
			),
			
			"template" => array(
			
				"name" => array(
				
					"index" => "index"
				
				),
				"path" => "./app/template/"
			
			),
			 
			"img" => array(

				"ext" => array(
					".jpg", ".jpeg", ".png", ".gif"
				)
			),
			
			"options" => array(
			
				"timeszone" => "Asia/Jakarta",
			),
			
			"dir" => array(
			
				"libs" => "./app/libs/",
				"dist" => "./app/assets/dist/",
				"plugins" => "./app/assets/plugins/"
			)
		);
		
		/* BACKEND CONFIGURATION */
		
		public static $backend = array(
		
			"controller" => array(
			
				"name" => "admin",
				"path" => "./app/public/"
			),
			
			"view" => array(
			
				"path" => "./app/public/admin/"
			
			),
			
			"template" => array(
			
				"name" => array(
					
					"index" => "admin",
					"login" => "admin_login"
				
				),
				"path" => "./app/template/"
			
			),
			
			"info" => array(
			
				"names" => "Freedoms",
				"version" => "0.0.1",
			),
		);
		
		public static $menu = array(
			
			"admin" => array(
			
				"dashboard" => array(
				
					"title" => "Dashboard",
					"icon" => "fa-dashboard",
					"access" => array("Super Administrator", "Administrator", "Moderator", "User"),
				),
				"account" => array(
				
					"title" => "Accounts",
					"icon" => "fa-lock",
					"access" => array("Super Administrator"),
					"submenu" => array(
					
						"list" => array(
						
							"title" => "List Accounts",
							"subtitle" => "Accounts list",
							"icon" => "fa-list",
							"visible" => true,
							"access" => array("Super Administrator", "Administrator"),
						),
						"add" => array(
						
							"title" => "Add Accounts",
							"subtitle" => "Accounts add",
							"icon" => "fa-user",
							"visible" => false,
							"access" => array("Super Administrator"),
						),
						"edit" => array(
						
							"title" => "Edit Accounts",
							"subtitle" => "Accounts edit",
							"icon" => "fa-user",
							"visible" => false,
							"access" => array("Super Administrator"),
						),
						"upload" => array(
						
							"title" => "Upload",
							"subtitle" => "Data",
							"icon" => "fa-user",
							"visible" => false,
							"access" => array("Super Administrator"),
						)
					)
				)
			)
		);
	}
	if(!defined('names')) exit('forbidden access');
}