<?php

/* 
 *	Freedoms version 0.0.1
 */
 
namespace Freedoms
{
	define ('names', 'Freedoms');
	
	if (version_compare (PHP_VERSION, '5.0.0', '>'))
	{
		require_once 'engines/Handler.php'; 
			new Handler ; 
				Handler :: development();
		require_once 'engines/Autoload.php';	
			new AutoLoad ; 
				AutoLoad :: load ('engines\Start');
	}
	else
	{
		exit ('<b style="color:#dcdcdc">' . names . ' required PHP 5.0 or higher</b>');
	}
}